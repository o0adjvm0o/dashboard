const Sequelize = require('sequelize')
const db        = {}
const sequelize = new Sequelize('ps_database', 'sa', 'Testsql2020', {
    dialect: 'mssql',
    host: 'localhost',
    dialectOptions: {
        encrypt: true
    }
});

sequelize.authenticate().then((res) => {
    console.log('Connect DB: OK');
})
.catch((err) => {
    console.log('Connect DB: FAIL', err);
})

db.sequelize = sequelize
db.Sequelize = Sequelize
module.exports = db