import React, { Component} from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Login from './views/Login/Login'
import Register from './views/Register/Register'
import Dashboard from './views/Dashboard/Dashboard'


class App extends Component {
  render() {
    return (
        <Router>
          <div className="App">
            <Route exact path="/" component={Login}/>
            <Route exact path="/register" component={Register}/>
            <Route  path="/dashboard" component={Dashboard}/>
          </div>
        </Router>
    );
  }
}

export default App
