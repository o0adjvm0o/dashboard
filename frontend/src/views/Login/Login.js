import React, { Component } from 'react'
import { login } from '../UserFuntions'
import './css/login.css'
import hologramlogin from './images/hologramLogin.png'
import { Redirect } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faGamepad, faLock } from '@fortawesome/free-solid-svg-icons'

class Login extends Component {
    constructor() {
        super()
        this.state = {
            userName: '',
            userPwd: ''
        }

        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }

    onChange(e) {
        this.setState( {[e.target.name]: e.target.value} )
    }

    onSubmit(e) {
        e.preventDefault()
        
        const user = {
            userName: this.state.userName,
            userPwd: this.state.userPwd
        }

        login(user).then(res => {
            if (res) {
                this.props.history.push('/dashboard/home')
            }
        })
    }


    render() {
        const token = localStorage.usertoken
        if (token) {
            return <Redirect to="/dashboard/home"/>
        } 
        return (
            <div className="container-fondo">
                <div className="container">
                    <div className="login-container">
                    <img src={hologramlogin} alt="hologramlogin" className="hologram-login"/>
                        <form noValidate onSubmit={this.onSubmit} className="formulario-login" >
                        
                            <div className="input-div one" >
                                <div className="i">
                                <FontAwesomeIcon icon={faGamepad}/>
                                </div>
                                <div>
                                    <input type="text" 
                                    name="userName"
                                    className="input"
                                    autoComplete="off" 
                                    autoFocus
                                    placeholder="Usuario"
                                    maxLength="10"
                                    value={this.state.userName}
                                    onChange={this.onChange}
                                    />
                                </div> 
                            </div>

                            <div className="input-div two" >
                                <div className="i">
                                <FontAwesomeIcon icon={faLock}/>
                                </div>
                                <div>
                                    <input type="password"
                                    className="input"
                                    name="userPwd"
                                    placeholder="Contraseña"
                                    maxLength="10"
                                    value={this.state.userPwd}
                                    onChange={this.onChange}
                                    />
                                </div>
                                
                            </div>
                            
                            <input className="btn" type="submit" value="ENTRAR"/>
                            
                            <a href="/register" className="link-login">Crea una cuenta!</a>

                            <div className="ErrorLogin" id="ErrorLogin"></div>
                        </form>
                    </div>
                </div>
            </div>
            
        )
    }
}

export default Login
