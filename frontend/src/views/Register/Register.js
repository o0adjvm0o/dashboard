import React, { Component } from 'react'
import { register } from '../UserFuntions'
import { Redirect } from 'react-router-dom'
import './css/register.css'
import hologramRegister from './images/hologramRegister.png'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSignature, faGamepad, faAt, faUser, faLock, faQuestion } from '@fortawesome/free-solid-svg-icons'

class Register extends Component {
    constructor() {
        super()
        this.state = {
            userFirstname: '',
            userLastname: '',
            userName: '',
            userEmail: '',
            userNick: '',
            userPwd: '',
            userPwdR: '',
            userQuestion: '',
            userResponse: '',
            userTerms: ''
        }

        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }

    onChange(e) {
        this.setState( {[e.target.name]: e.target.value} )
    }

    onSubmit(e) {
        e.preventDefault()

        const user = {
            userFirstName: this.state.userFirstName,
            userLastName: this.state.userLastName,
            userName: this.state.userName,
            userEmail: this.state.userEmail,
            userNick: this.state.userNick,
            userPwd: this.state.userPwd,
            userPwdR: this.state.userPwdR,
            userQuestion: this.state.userQuestion,
            userResponse: this.state.userResponse,
            userTerms: this.state.userTerms
        }

        register(user).then(res => {
            if (res) {
                this.props.history.push('/dashboard/home')
            }
        })
    }

    render() {
        const token = localStorage.usertoken
        if (token) {
            return <Redirect to="/dashboard/home"/>
        }
        return (
            <div className="container-fondo-register">
                <div className="container-register">
                    <div className="register-container">
                    <img src={hologramRegister} alt="hologramRegister" className="hologram-register"/>
                        <form noValidate onSubmit={this.onSubmit} className="formulario-register">
                            
                            <div className="input-div-register" >
                                <div className="iF">
                                    <FontAwesomeIcon icon={faSignature}/>
                                </div>
                                <div>
                                    <input type="text" 
                                    name="userFirstName"
                                    className="input-register-F"
                                    autoComplete="off" 
                                    autoFocus
                                    placeholder="Primer Nombre"
                                    maxLength="30"
                                    value={this.state.userFirstName}
                                    onChange={this.onChange}
                                    />
                                </div>
                            </div>

                            <div className="input-div-register">
                                <div className="iL">
                                    <FontAwesomeIcon icon={faSignature}/>
                                </div>
                                <div>
                                    <input type="text" 
                                    name="userLastName"
                                    className="input-register-L"
                                    autoComplete="off"
                                    placeholder="Apellidos"
                                    maxLength="30"
                                    value={this.state.userLastName}
                                    onChange={this.onChange}
                                    />
                                </div>
                            </div>

                            
                            <div className="input-div-register">
                            <div className="iU">
                                <FontAwesomeIcon icon={faGamepad}/>
                            </div>
                                <div>
                                    <input type="text" 
                                    name="userName"
                                    className="input-register-U"
                                    autoComplete="off" 
                                    placeholder="Usuario"
                                    maxLength="10"
                                    value={this.state.userName}
                                    onChange={this.onChange}
                                    />
                                </div>
                            </div>

                            
                            <div className="input-div-register"> 
                            <div className="iE">
                                <FontAwesomeIcon icon={faAt}/>
                            </div>
                                <div>
                                    <input type="email"
                                    className="input-register-E"
                                    name="userEmail"
                                    placeholder="Correo"
                                    maxLength="40"
                                    value={this.state.userEmail}
                                    onChange={this.onChange}
                                    />
                                </div>
                            </div>

                              
                            <div className="input-div-register"> 
                            <div className="iN">
                                <FontAwesomeIcon icon={faUser}/>
                            </div>  
                                <div>
                                    <input type="text"
                                    className="input-register-N"
                                    name="userNick"
                                    placeholder="Que apodo deseas tener"
                                    maxLength="40"
                                    value={this.state.userNick}
                                    onChange={this.onChange}
                                    />
                                </div>
                            </div>

                            
                            <div className="input-div-register">
                            <div className="iP">
                                <FontAwesomeIcon icon={faLock}/>
                            </div>
                                <div>
                                    <input type="password"
                                    className="input-register-P"
                                    name="userPwd"
                                    placeholder="Contraseña"
                                    maxLength="10"
                                    value={this.state.userPwd}
                                    onChange={this.onChange}
                                    />
                                </div>
                            </div>

                            
                            <div className="input-div-register"> 
                            <div className="iRP">
                                <FontAwesomeIcon icon={faLock}/>
                            </div>
                                <div>
                                    <input type="password"
                                    className="input-register-RP"
                                    name="userPwdR"
                                    placeholder="Repetir contraseña"
                                    maxLength="10"
                                    value={this.state.userPwdR}
                                    onChange={this.onChange}
                                    />
                                </div>
                            </div>


                            <select className="input-register-Q"
                                    name="userQuestion"
                                    value={this.state.userQuestion}
                                    onChange={this.onChange}>
                                <option value="">Seleccionar una pregunta</option>
                                <option value="Como se llama tu mejor amigo">Como se llama tu mejor amigo!</option>
                                <option value="Cual es tu juego favorito">Cual es tu juego favorito!</option>
                                <option value="Cual es el nombre de tu perro">Cual es el nombre de tu perro!</option>
                                <option value="Cual es el nombre de tu primo">Cual es el nombre de tu primo!</option>
                                <option value="Cual es tu anime favorito">Cual es tu anime favorito!</option>
                                <option value="Cual es el nombre de tu crush">Cual es el nombre de tu crush!</option>
                                <option value="Cual es el pais que mas te gusta">Cual es el pais que mas te gusta!</option>
                            </select>

                            
                            <div className="input-div-register"> 
                            <div className="iR">
                                <FontAwesomeIcon icon={faQuestion}/>
                            </div>
                                <div>
                                    <input type="text"
                                    className="input-register-R"
                                    name="userResponse"
                                    placeholder="Respuesta"
                                    maxLength="40"
                                    value={this.state.userResponse}
                                    onChange={this.onChange}
                                    />
                                </div>
                            </div>

                            <div>
                                <input type="checkbox"
                                className="input-terms"
                                name="userTerms"
                                checked={this.state.userTerms}
                                onChange={this.onChange}
                                />
                                <label className="label-terms"> ¿ Acepta todos los terminos y condiciones del juego ?</label>
                            </div>

                            <input className="btn-register"  type="submit" value="REGISTRAR"/>

                            <div className="link-register">
                                <a href="/" >Ya tengo una cuenta!</a>
                            </div>
                            
                            <div className="ErrorRegister" id="ErrorRegister"></div>
                        </form>
                    </div>
                </div>
            </div>
            
        )
    }
}

export default Register
