import React, { Component } from 'react';

//CSS
import './css/download.css'

class Download extends Component {
  render() {
    return (
      <div className="container-download">
        <div className="item-mediafire">
          <img className="img" src={require('./images/mediafire.png')} alt="mediafire"></img>
          <label className="text-m">DESCARGAR POR MEDIAFIRE</label>
        </div>

        <div className="item-mega">
          <img className="img" src={require('./images/mega.png')} alt="mega"></img>
          <label className="text-me">DESCARGAR POR MEGA</label>
        </div>

        <div className="item-google">
          <img className="img" src={require('./images/google.png')} alt="google"></img>
          <label className="text-g">DESCARGAR POR GOOGLE DRIVE</label>
        </div>
      </div>
    )
  }
}

export default Download